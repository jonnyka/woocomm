<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */
if (!defined('ABSPATH')) {
	exit;
}

if ($related_products): ?>
    <?php
    $terms = wp_get_post_terms(get_the_id(), 'product_tag');
    $termName = '';

    if (count($terms) > 0) {
        foreach ($terms as $term) {
            $termName = $term->name;
        }
    }

    $title = 'Other products of the design';
    if ($termName) {
        $title = $termName . ' products';
    }

    $pids = array();
    foreach ($related_products as $related_product):
        $pids[] = $related_product->get_id();
    endforeach;
    ?>
	<section class="related products">
		<h2><?php echo $title ?></h2>

		<?php
        woocommerce_product_loop_start();
            $args = array(
                'post_type' => 'product',
                'posts_per_page' => '-1',
                'orderby' => 'menu_order',
                'order' =>  'ASC',
                'post__in' => $pids,
            );
            $loop = new WP_Query($args);
            while ($loop->have_posts()): $loop->the_post();
                wc_get_template_part('content', 'product');
            endwhile;
		woocommerce_product_loop_end(); ?>
	</section>
<?php endif;

wp_reset_postdata();
