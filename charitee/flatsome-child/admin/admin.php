<?php

include_once 'printful.php';

/**
 * Adds 'Set selectors' button to the product edit page.
 *
 * @param $hook
 */
function add_admin_scripts($hook) {
    global $post;

    if ($hook === 'post.php' && 'product' === $post->post_type) {
        wp_enqueue_style('wp-color-picker');
        wp_enqueue_script('wp-color-picker');
        wp_enqueue_script('setSelectors', get_stylesheet_directory_uri() . '/admin/setSelectors.js?ver=' . time());
    }
}
add_action('admin_enqueue_scripts', 'add_admin_scripts', 10, 1);

/**
 * Adds 'Donation' column header to 'Orders' page immediately after 'Total' column.
 *
 * @param string[] $columns
 * @return string[] $new_columns
 */
function charitee_add_order_donation_column_header($columns) {
    $new_columns = array();

    foreach ($columns as $column_name => $column_info) {
        $new_columns[$column_name] = $column_info;

        if ('order_total' === $column_name) {
            $new_columns['order_donation'] = __('Donation', 'my-textdomain');
        }
    }

    return $new_columns;
}
add_filter('manage_edit-shop_order_columns', 'charitee_add_order_donation_column_header', 20);

if (!function_exists('charitee_get_order_meta')) {
    /**
     * Helper function to get meta for an order.
     *
     * @param \WC_Order $order   the order object
     * @param string    $key     the meta key
     * @param bool      $single  whether to get the meta as a single item. Defaults to `true`
     * @param string    $context if 'view' then the value will be filtered
     *
     * @return mixed the order property
     */
    function charitee_get_order_meta($order, $key = '', $single = TRUE, $context = 'edit') {
        if (defined('WC_VERSION') && WC_VERSION && version_compare(WC_VERSION, '3.0', '>=')) {
            $value = $order->get_meta($key, $single, $context);
        }
        else {
            $order_id = is_callable(array($order, 'get_id')) ? $order->get_id() : $order->id;
            $value = get_post_meta($order_id, $key, $single);
        }

        return $value;
    }
}

/**
 * Adds 'Domain' column content to 'Orders' page immediately after 'Total' column.
 *
 * @param string[] $column name of column being displayed
 */
function charitee_add_order_donation_column_content($column) {
    global $post;

    if ('order_donation' === $column) {
        $order    = wc_get_order($post->ID);
        $currency = is_callable(array($order, 'get_currency')) ? $order->get_currency() : $order->order_currency;
        $donation = charitee_get_order_meta($order, 'fp_donation_value');

        if ('' !== $donation || false !== $donation) {
            $donation = (float)$donation;
        }

        echo wc_price($donation, array('currency' => $currency));
    }
}
add_action('manage_shop_order_posts_custom_column', 'charitee_add_order_donation_column_content');

add_action('admin_head', 'custom_admin_css');

function custom_admin_css() {
    echo '<style>
    #thumbler-menu {
        display: none
    }
    ul#adminmenu {
        padding-top: 0;
    }
    .notice {
        display: none;
    }
    
    .expansion-alids-init #thumbler-menu {
        display: block;
    }
    .expansion-alids-init ul#adminmenu {
        padding-top: 35px;
    }
    .expansion-alids-init .notice {
        display: block;
    }
  </style>';
}