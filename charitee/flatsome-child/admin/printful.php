<?php

$partners = [
    'mirarae' => [
        'enabled'    => false,
        'shortName'  => 'mirarae',
        'longName'   => 'MiraRae',
        'productTag' => 'mirarae',
        'payments'   => [],
    ],
    'jentcc' => [
        'enabled'    => true,
        'shortName'  => 'jentcc',
        'longName'   => 'JenTCC',
        'productTag' => 'JenTCC',
        'payments'   => [
            ['amount' => 22.53, 'date' => '11/05/2020'],
        ],
    ],
];

/**
 * Create the custom user role
 */
function charitee_user_role() {
    foreach ($GLOBALS['partners'] as $partner) {
        if ($partner['enabled']) {
            add_role(
                $partner['shortName'] . '_role',
                $partner['longName'] . ' Role',
                [
                    'read' => true,
                    'edit_posts' => true,
                ]
            );
        }
    }
}

add_action('init', 'charitee_user_role');

/**
 * Create custom role capabilities
 */
function charitee_user_role_caps() {
    foreach ($GLOBALS['partners'] as $partner) {
        if ($partner['enabled']) {
            $role = get_role($partner['shortName'] . '_role');
            $role->add_cap($partner['shortName'] . '_cap', true);
            $role = get_role('administrator');
            $role->add_cap($partner['shortName'] . '_cap', true);
        }
    }
}

// add example_role capabilities, priority must be after the initial role
add_action('init', 'charitee_user_role_caps', 11);

/**
 * Add the custom collab admin top menus
 *
 * @param $wp_admin_bar
 */
function charitee_admin_top_menu($wp_admin_bar) {
    foreach ($GLOBALS['partners'] as $partner) {
        if ($partner['enabled']) {
            $args = array (
                'id'    => $partner['shortName'] . '-admin-page',
                'title' => '<span style="font-size: 16px; margin-top: 4px;" class="ab-icon dashicons dashicons-list-view"></span>' . $partner['longName'] . ' orders',
                'href'  => 'https://charitee.life/wp-admin/admin.php?page=' . $partner['shortName'] . '-admin-page',
            );
            $wp_admin_bar->add_node($args);
        }
    }
}

add_action('admin_bar_menu', 'charitee_admin_top_menu', 999);

/**
 * Add the collab admin menus
 */
function charitee_admin_menu() {
    foreach ($GLOBALS['partners'] as $partner) {
        if ($partner['enabled']) {
            add_menu_page(
                __($partner['longName'] . ' orders', 'my-textdomain'),
                __($partner['longName'] . ' orders', 'my-textdomain'),
                $partner['shortName'] . '_cap',
                $partner['shortName'] . '-admin-page',
                'charitee_admin_page_contents',
                'dashicons-list-view',
                3
            );
        }
    }
}

add_action('admin_menu', 'charitee_admin_menu');

/**
 * Get orders from Printful
 *
 * @param $status
 * @return mixed
 */
function getOrders($status) {
    $host = 'https://api.printful.com/orders'; //?status=' . $status; // ?status=fulfilled
    $authorization = "Authorization: Basic dHRtdGtsdGgtOGRsZS04YmZnOjJscHMtODYzeTk2dGJkMndt";

    $ch = curl_init($host);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $result = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($result, true);
    $data = $data['result'];

    return $data;
}

/**
 * Custom collab admin menu functionality
 */
function charitee_admin_page_contents() {
    $funcName = $_GET['page'];
    $partnerId = str_replace('-admin-page', '', $funcName);
    $partner = $GLOBALS['partners'][$partnerId];

    setlocale(LC_MONETARY, 'en_US.UTF-8');

    /*
    $fulfilled = getOrders('fulfilled');
    $pending = getOrders('pending');
    $data = array_merge($fulfilled, $pending);
    */
    $data = getOrders('');

    $vids = array();
    //$args = array('post_type' => 'product', 'product_tag' => 'mirarae');
    $args = array('post_type' => 'product', 'product_tag' => $partner['productTag']);
    $loop = new WP_Query($args);
    while ($loop->have_posts()):
        $loop->the_post();
        global $product;
        global $post;

        $args = array(
            'post_type' => 'product_variation',
            'post_status' => array('private', 'publish'),
            'numberposts' => -1,
            'orderby' => 'menu_order',
            'order' => 'asc',
            'post_parent' => get_the_ID()
        );
        $variations = get_posts($args);

        foreach ($variations as $variation) {
            $vids[] = $variation->ID;
        }
    endwhile;
    //echo '<pre>'; print_r($data); echo '</pre>';

    $partnerData = array();
    $partnerSumData = array(
        'sumBuyPrice' => 0,
        'sumOurPrice' => 0,
        'sumProfit' => 0,
        'sumPartnerMoney' => 0,
        'sumPayed' => 0,
        'sumRemaining' => 0,
    );
    foreach ($data as $d) {
        //echo '<pre>-------------------'; print_r($d['items']); echo '</pre>';
        $mehet = true;
        if ($d['status'] === 'canceled' || $d['status'] === 'archived') {
            $mehet = false;
        }
        foreach ($d['items'] as $item) {
            if (!$item['external_variant_id'] || !in_array($item['external_variant_id'], $vids)) {
                $mehet = false;
            }
        }

        if ($mehet) {
            $partnerData[$d['created']] = array();
            $f = &$partnerData[$d['created']];
            $recip = $d['recipient'];

            $f['id'] = $d['id'];
            $f['recipName'] = $recip['name'];
            $f['recipAddr'] = $recip['country_name'] . ' - ' . $recip['city'];
            $f['date'] = date('d/m/y H:i:s', $d['created']);
            $f['items'] = '';
            $f['buyPrices'] = '';
            $f['ourPrices'] = '';
            $f['vat'] = $d['costs']['vat'];
            $f['tax'] = $d['costs']['tax'];
            $f['sumBuyPrice'] = $d['retail_costs']['subtotal'];
            if ($d['id'] !== 32610470) {
                $f['sumBuyPrice'] += $f['tax'];
            }
            $f['sumOurPrice'] = $d['costs']['subtotal'] + $f['tax'] + $f['vat'];
            $f['profit'] = $f['sumBuyPrice'] - $f['sumOurPrice'];
            $f['partnerMoney'] = 0.40 * $f['profit'];
            $partnerSumData['sumBuyPrice'] += $f['sumBuyPrice'];
            $partnerSumData['sumOurPrice'] += $f['sumOurPrice'];
            $partnerSumData['sumProfit'] += $f['profit'];
            $partnerSumData['sumPartnerMoney'] += $f['partnerMoney'];

            foreach ($d['items'] as $item) {
                $last = end($item['files']);
                $image = $last['preview_url'];
                $f['items'] .= '<div class="hover_img"><a href="#">' . $item['name'] . '<span><img src="' . $image . '" alt="image" height="300" /></span></a></div>';
                //$f['items'] .= $item['name'] . '<br />';
                $f['buyPrices'] .= money_format('%.2n', $item['retail_price']) . '<br />';
                $f['ourPrices'] .= money_format('%.2n', $item['price']) . '<br />';
            }
            $f['items'] = rtrim($f['items'], '<br />');
        }
    }

    foreach ($partner['payments'] as $payment) {
        $partnerSumData['sumPayed'] += $payment['amount'];
    }
    $partnerSumData['sumRemaining'] = $partnerSumData['sumPartnerMoney'] - $partnerSumData['sumPayed'];

    //echo '<pre>-------------------'; print_r($partnerData); echo '</pre>';

    ?>
    <h1>
        <?php
        ?>
        <?php esc_html_e($partner['longName'] . ' orders page', 'my-textdomain'); ?>
    </h1>
    <style>
        td .hover_img a {
            position: relative;
            cursor: default;
        }

        td .hover_img a span {
            position: absolute;
            display: none;
            z-index: 99;
            left: -310px;
            top: -50px;
            border: 2px solid #333;
        }

        td .hover_img a span img {
            vertical-align: middle;
        }

        td .hover_img a:hover span {
            display: block;
        }

        @media screen and (max-width: 782px) {
            #jentcc-table th,
            #jentcc-table td {
                font-size: .5em;
            }
        }
    </style>
    <table id="<?php echo $partner['shortName']; ?>-table" class="widefat fixed striped posts">
        <thead>
        <tr>
            <th class="manage-column column-primary">Date</th>
            <th class="manage-column">Customer name</th>
            <th class="manage-column">Customer address</th>
            <th class="manage-column">Items</th>
            <th class="manage-column">Customer payed</th>
            <th class="manage-column">Our costs</th>
            <th class="manage-column">Our profit</th>
            <th class="manage-column">Your 40% share</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($partnerData as $partnerOutput) {
            ?>
            <tr class="author-self level-0 type-shop_order hentry">
                <td><?php echo $partnerOutput['date']; ?></td>
                <td><?php echo $partnerOutput['recipName']; ?></td>
                <td><?php echo $partnerOutput['recipAddr']; ?></td>
                <td><?php echo $partnerOutput['items']; ?></td>
                <td>
                    <?php echo $partnerOutput['buyPrices']; ?>
                    <?php if ($partnerOutput['id'] !== 32610470) { ?>
                        TAX: <?php echo money_format('%.2n', $partnerOutput['tax']); ?><br/>
                    <?php } ?>
                    Total: <?php echo money_format('%.2n', $partnerOutput['sumBuyPrice']); ?></td>
                <td>
                    <?php echo $partnerOutput['ourPrices']; ?>
                    <?php if ($partnerOutput['vat'] !== '0.00') { ?>VAT: <?php echo money_format('%.2n', $partnerOutput['vat']); ?>
                        <br/><?php } ?>
                    <?php if ($partnerOutput['tax'] !== '0.00') { ?>TAX: <?php echo money_format('%.2n', $partnerOutput['tax']); ?>
                        <br/><?php } ?>
                    <?php if (money_format('%.2n', $partnerOutput['sumOurPrice']) !== $partnerOutput['ourPrices']) { ?>Total: <?php echo money_format('%.2n', $partnerOutput['sumOurPrice']); ?><?php } ?>
                </td>
                <td><?php echo money_format('%.2n', $partnerOutput['profit']); ?></td>
                <td><?php echo money_format('%.2n', $partnerOutput['partnerMoney']); ?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <th class="manage-column column-primary">SUM</th>
            <th class="manage-column"></th>
            <th class="manage-column"></th>
            <th class="manage-column"></th>
            <th class="manage-column"><?php echo money_format('%.2n', $partnerSumData['sumBuyPrice']); ?></th>
            <th class="manage-column"><?php echo money_format('%.2n', $partnerSumData['sumOurPrice']); ?></th>
            <th class="manage-column"><?php echo money_format('%.2n', $partnerSumData['sumProfit']); ?></th>
            <th class="manage-column"><?php echo money_format('%.2n', $partnerSumData['sumPartnerMoney']); ?></th>
        </tr>
        </tfoot>
    </table>

    <h3>Payments:</h3>

    <table id="<?php echo $partner['shortName']; ?>-table-payments" class="widefat fixed striped posts">
        <thead>
        <tr>
            <th class="manage-column column-primary">Date</th>
            <th class="manage-column">Amount</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($partner['payments'] as $payment) {
            ?>
            <tr class="author-self level-0 type-shop_order hentry">
                <td><?php echo $payment['date']; ?></td>
                <td><?php echo money_format('%.2n', $payment['amount']); ?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <th class="manage-column column-primary">SUM</th>
            <th class="manage-column"><?php echo money_format('%.2n', $partnerSumData['sumPayed']); ?></th>
        </tr>
        <tr>
            <th class="manage-column column-primary">Remaining</th>
            <th class="manage-column"><?php echo money_format('%.2n', $partnerSumData['sumRemaining']); ?></th>
        </tr>
        </tfoot>
    </table>
    <?php
}