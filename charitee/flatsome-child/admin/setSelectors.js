var $ = jQuery;

function setSelectors() {
    var colors = {
        "White": "#E2E3DE",
        "Black": "#27262B",
        "Brown": "#372D2C",
        "Black Heather": "#2A282B",
        "Heather Forest": "#4F5549",
        "Heather Midnight Navy": "#35353F",
        "Olive": "#434C31",
        "Asphalt": "#505457",
        "Navy": "#37384A",
        "Forest": "#1F4A2E",
        "Dark Grey Heather": "#3E3C3D",
        "Dark Heather": "#425563",
        "Army": "#836652",
        "Leaf": "#548655",
        "Heather Deep Teal": "#426275",
        "Steel Blue": "#4D657D",
        "Oxblood Black": "#190406",
        "Athletic Heather": "#A8ABB2",
        "Silver": "#B8BCBF",
        "Kelly": "#016D56",
        "Mauve": "#BF6E6E",
        "Soft Cream": "#D3C4AD",
        "Heather Prism Dusty Blue": "#A1C3B8",
        "Ash": "#EEF0F2",
        "Heather Prism Mint": "#AAD4B7",
        "True Royal": "#18498C",
        "Heather Blue": "#86A9C9",
        "Heather Prism Peach": "#EEC1B3",
        "Heather Prism Ice Blue": "#C0E3E4",
        "Ocean Blue": "#619DC1",
        "Aqua": "#5191BD",
        "Heather Mint": "#72D3B4",
        "Heather Orange": "#D96E51",
        "Yellow": "#FBF271",
        "Heather True Royal": "#5F98E6",
        "Heather Prism Lilac": "#D9B0CB",
        "Gold": "#F8A933",
        "Red": "#A02331",
        "Berry": "#C13C7E",
        "Pink": "#FCD1DB",
        "Heather Raspberry": "#FC667D",
        "Heather Navy": "#2E313E",
        "Military Green": "#5B5537",
        "Indigo Blue": "#4E5C75",
        "Sport Grey": "#9B969C",
        "Irish Green": "#1E8043",
        "Maroon": "#471924",
        "Heather Dark Grey": "#262F36",
        "Heather Grey": "#928D92",
        "Caribbean Blue": "#008FB7",
        "Green Camo": "#415446",
        "Khaki": "#CCBCAD",
        "Stone": "#DCD5CD",
        "Light Blue": "#83AED1",
        "Black/ Teal": "#252235",
        "Black/ Neon Pink": "#25211E",
        "Black/ Red": "#25292E",
        "Black/ Silver" : "#24292F",
        "Dark Grey": "#666B64",
        "Navy/ Red": "#35435E",
        "Spruce": "#013423",
        "Heather/Black": "#A8A59E",
        "Heather Grey/ Navy": "#A8A59E",
        "Heather Grey/ Red": "#A8A59E",
        "Royal/ Orange": "#39458F",
        "Royal Blue": "#333290",
        "Natural/ Black": "#E7E1CB",
        "Sand": "#D4CAB4",
        "Royal": "#295293",
        "Orange": "#E34D31",
        "Light Pink": "#F6CED6",
        "Charcoal-black Triblend": "#404040",
        "Oatmeal Triblend": "#FFF9ED",
        "Red Triblend": "#D62D34",
        "Heather Columbia Blue": "#0092D5",
	"Bright Pink": "#FF66B6"
    };

    function setStuff() {
        $('a[href="#colored_variable_tab_data"]').click();
        $('#colored_variable_tab_data').find('.accordion-header').each(function() {
            var dat = $(this),
                cont = dat.next('.accordion-content'),
                curLabel = dat.find('.attribute-label').text().trim(),
                isParent = curLabel.toLowerCase().indexOf('color') > -1 || curLabel.toLowerCase().indexOf('size') > -1,
                isCol = cont.find('select[name="coloredvariables[Color][display_type]"]').length === 1 || cont.find('select[name="coloredvariables[Handle color][display_type]"]').length === 1;

            if (isParent) {
                dat.click();
                cont.find('option:contains("olor")').prop('selected', true).trigger('change');
                cont.find('.wcvaimageorcolordiv').find('.accordion-header').each(function() {
                    $(this).click();
                });

                if (isCol) {
                    cont.find('select[name="coloredvariables[Color][show_name]"] option:contains("Yes")').prop('selected', true).trigger('change');
                    cont.find('select[name="coloredvariables[Handle color][show_name]"] option:contains("Yes")').prop('selected', true).trigger('change');

                    cont.find('.wcvaimageorcolordiv').find('.accordion-container').each(function() {
                        var curCol = $(this).find('.attribute-label').text().trim(),
                            but = $(this).find('.wp-color-result');

                        if (but.length) {
                            but.click();
                            $(this).find('.wp-color-picker').val(colors[curCol]).trigger('change');
                            but.css('background-color', colors[curCol]);
                            but.click();
                        } else {
                            $(this).find('.wcvaattributecolorselect').trigger('blur').val(colors[curCol]).trigger('blur').trigger('change');
                        }
                    });
                } else {
                    cont.find('.wcvaimageorcolordiv').find('.accordion-container').each(function() {
                        $(this).find('.wcvacolororimage option:contains("Text")').prop('selected', true).trigger('change');
                    });
                }
            }
        });
    }

    function saveStuff() {
        $('#set-selectors').prop('disabled', true);
        $('#publish').click();
    }

    setStuff();
    saveStuff();
}

setTimeout(function() {
    if (!$('#set-selectors').length) {
        $('#publishing-action').after('<a style="margin-top: 10px; background: #8BC34A; color: #fff;" class="clear preview button" href="#" id="set-selectors">Set selectors</a>');
        $('#set-selectors').click(setSelectors);
    }
}, 1000);
