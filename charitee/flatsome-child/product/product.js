setTimeout(function () {
    var $ = jQuery,
        labelM = $('label[data-option="M"]'),
        labelMisc = $('label[selectid="size"]'),
        labelColor = $('label[selectid="color"]'),
        empty;

    if (labelM.length && !labelM.hasClass('selectedswatch')) {
        labelM.trigger('click');
    } else if (labelMisc.length && !labelMisc.hasClass('selectedswatch')) {
        $(labelMisc[0]).trigger('click');
    }

    if (labelColor.length && !labelColor.hasClass('selectedswatch')) {
        $(labelColor[0]).trigger('click');
    }

    $('.product-section').each(function () {
        empty = true;

        $(this).find('div').each(function() {
            if ($(this).text().trim().length) {
                empty = false;
            }
        });

        if (empty || $(this).find('.entry-content:empty').length) {
            $(this).hide();
        }
    });
}, 1000);
