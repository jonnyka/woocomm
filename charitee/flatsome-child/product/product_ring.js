setTimeout(function () {
    var $ = jQuery,
        select1 = $('.adsw-attribute-option .meta-item-text:contains("1")'),
        selectSilver = $('.adsw-attribute-option .meta-item-text:contains("Silver")'),
        selectGold = $('.adsw-attribute-option .meta-item-text:contains("Gold")'),
        selectRoseGold = $('.adsw-attribute-option .meta-item-text:contains("Rose Gold")');

    $('.woocommerce-variation-add-to-cart').insertAfter('.woocommerce-variation');
    selectSilver.addClass('select-silver');
    selectGold.addClass('select-gold');
    selectRoseGold.removeClass('select-gold').addClass('select-rose-gold');

    function toggleSecondFields() {
        var fields = $('.product-addon-name-2, .product-addon-gem-2');

        if (select1.hasClass('active')) {
            fields.prop('required', false).hide();
        } else {
            fields.prop('required', true).show();
        }
    }

    toggleSecondFields();
    $('.meta-item-text').click(toggleSecondFields);
}, 1000);