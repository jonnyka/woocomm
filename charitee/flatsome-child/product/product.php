<?php

function reordered_tabs($tabs) {
    unset($tabs['additional_information']);
    $tabs['description']['priority'] = 10;
    $tabs['size_chart']['priority'] = 15;
    $tabs['reviews']['priority'] = 20;

    return $tabs;
}
add_filter('woocommerce_product_tabs', 'reordered_tabs', 98);

add_filter('wc_product_sku_enabled', '__return_false');

function add_product_script() {
    if (is_product()) {
        global $product;
        wp_enqueue_script('productjs', get_stylesheet_directory_uri() . '/product/product.js');
        // wp_enqueue_script('slickjs', get_stylesheet_directory_uri() . '/slick/slick.min.js');
        // wp_enqueue_style('slickcss', get_stylesheet_directory_uri() . '/slick/slick.css');

        if ($product->get_id() == 46030) {
            wp_enqueue_script('productringjs', get_stylesheet_directory_uri() . '/product/product_ring.js');
        }
    }
}
add_action('wp_enqueue_scripts', 'add_product_script');

function remove_related_product_categories($terms_ids, $product_id) {
    return array();
}
add_filter('woocommerce_get_related_product_cat_terms', 'remove_related_product_categories', 10, 2);

/*
function custom_wc_ajax_variation_threshold($qty, $product) {
    return 80;
}
add_filter('woocommerce_ajax_variation_threshold', 'custom_wc_ajax_variation_threshold', 10, 2);
*/