<?php

function add_checkout_script() {
    echo '<script src="' . get_stylesheet_directory_uri() . '/checkout/checkout.js?ver=' . time() . '" type="text/javascript"></script>';
}
add_action('woocommerce_after_checkout_form', 'add_checkout_script');