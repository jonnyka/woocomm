var $ = jQuery;

if ($('.browse-all-designs').length) {
    $('.browse-all-designs').off('click').on('click', function () {
        $('html, body').animate({scrollTop: $('#designs').offset().top - 200}, 1000);
    });
}
