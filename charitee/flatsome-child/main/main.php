<?php

function add_main_script() {
    if (is_page('41224')) {
        echo '<script src="' . get_stylesheet_directory_uri() . '/main/main.js" type="text/javascript"></script>';
    }
}
add_action('wp_head', 'add_main_script');

function custom_woocommerce_product_subcategories_args($args) {
    $args['exclude'] = get_option('default_product_cat');
    return $args;
}
add_filter('woocommerce_product_subcategories_args', 'custom_woocommerce_product_subcategories_args');

add_filter('jpeg_quality', function($arg) {
    return 100;
});
