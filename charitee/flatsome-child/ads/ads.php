<?php

add_action('wp_footer', 'add_pinterest_tracking', 20);

function add_pinterest_tracking() {
    if (is_page('checkout')) { ?>
        <script>
            pintrk('track', 'checkout', {
                value: 100,
                order_quantity: 1,
                currency: 'USD'
            });
        </script>
    <?php }
}
