<?php

function obfuscate_email($email) {
    $limit = 4;
    $em    = explode('@', $email);
    $name  = implode(array_slice($em, 0, count($em) - 1), '@');
    $len   = floor(strlen($name) / 2);
    if ($len > $limit) {
        $len = $limit;
    }

    return substr($name, 0, $len) . str_repeat('*', strlen($name) - $len) . '@' . end($em);
}

function shortcode_all_orders() {
    $query = new WC_Order_Query(array(
        // 'date_created' => '>' . (time() - 604800),
        'orderby' => 'date',
        'order' => 'DESC',
    ));
    $orders = $query->get_orders();
    $ret = array();

    foreach ($orders as $order) {
        $currency = is_callable(array($order, 'get_currency')) ? $order->get_currency() : $order->order_currency;
        $donation = (float)$order->get_meta('fp_donation_value');
        $target = '';
        $fees = $order->get_fees();
        foreach ($fees as $fee) {
            $meta = $fee->get_meta_data();
            foreach ($meta as $m) {
                $metaData = $m->get_data();

                if ($metaData['key'] === '_wc_checkout_add_on_value') {
                    $target = $metaData['value'];

                    if (!array_key_exists($target, $ret)) {
                        $ret[$target] = array();
                    }
                }
            }
        }

        if ($target) {
            $ret[$target][] = array(
                'email' => obfuscate_email($order->get_billing_email()),
                'date' => $order->get_date_created()->format('d-m-Y'),
                'amount' => wc_price($donation, array('currency' => $currency)),
                'rawamount' => $donation,
                'currency' => $currency,
            );
        }
    }

    ob_start();

    echo '<h3 class="accordion_title">Donations</h3>';
    echo '<div class="accordion" rel="">';

    foreach ($ret as $targetId => $targetOrders) {
        $total = 0;
        echo '<div class="accordion-item"><a href="#" class="accordion-title plain"><button class="toggle"><i class="icon-angle-down"></i></button><span>';
        if ($targetId === 'american-cancer-society') {
            echo 'American Cancer Society';
        } elseif ($targetId === 'american-red-cross') {
            echo 'American Red Cross';
        } elseif ($targetId === 'unicef') {
            echo 'Unicef';
        }

        echo '</span></a><div class="accordion-inner"><p>';

        echo '<table><thead><tr><th>Buyer</th><th>Amount</th><th>Date</th></tr></thead><tbody>';

        foreach ($targetOrders as $targetOrder) {
            $total += $targetOrder['rawamount'];

            echo '<tr>';
            echo '<td>' . $targetOrder['email'] . '</td>';
            echo '<td>' . $targetOrder['amount'] . '</td>';
            echo '<td>' . $targetOrder['date'] . '</td>';
            echo '</tr>';
        }

        echo '</tbody><tfoot><tr><td><b>Total</b></td><td>' . wc_price($total, array('currency' => $targetOrder['currency'])) . '</td><td></td></tr></tfoot></table>';
        echo '</p></div></div>';
    }

    echo '</div>';

    return ob_get_clean();
}
add_shortcode('all_orders', 'shortcode_all_orders');
