<?php

add_filter('wc_product_sku_enabled', '__return_false');

function remove_related_product_categories($terms_ids, $product_id) {
    return array();
}
add_filter('woocommerce_get_related_product_cat_terms', 'remove_related_product_categories', 10, 2);

function add_product_script() {
    if (is_product()) {
        wp_enqueue_script('productjs', get_stylesheet_directory_uri() . '/product/product.js');
    }
}
add_action('wp_enqueue_scripts', 'add_product_script');