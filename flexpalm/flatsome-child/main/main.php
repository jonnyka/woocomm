<?php

function add_main_script() {
    echo '<script src="' . get_stylesheet_directory_uri() . '/main/main.js" type="text/javascript"></script>';
}
add_action('wp_head', 'add_main_script');
